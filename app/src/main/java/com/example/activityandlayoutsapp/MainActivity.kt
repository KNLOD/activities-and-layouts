package com.example.activityandlayoutsapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.core.os.bundleOf
import com.google.android.material.textfield.TextInputEditText


class MainActivity : AppCompatActivity() {

    companion object{
        val LAST_NAME = "last_name"
        val FIRST_NAME = "first name"
        val FATHERS_NAME = "fathers name"
        val AGE = "age"
        val HOBBIES = "hobbies"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val last_name_TextEdit = findViewById<TextInputEditText>(R.id.last_name)
        val first_name_TextEdit = findViewById<TextInputEditText>(R.id.first_name)
        val fathers_name_TextEdit = findViewById<TextInputEditText>(R.id.fathers_name)
        val age_TextEdit = findViewById<TextInputEditText>(R.id.age)
        val hobbies_TextEdit = findViewById<TextInputEditText>(R.id.hobbies)
        val enter_button = findViewById<Button>(R.id.enter_button)

        val intent = Intent(this, InfoActivity::class.java)

        enter_button.setOnClickListener{
           intent.putExtras(bundleOf(
                    LAST_NAME to last_name_TextEdit.text.toString(),
                    FIRST_NAME to first_name_TextEdit.text.toString(),
                    FATHERS_NAME to fathers_name_TextEdit.text.toString(),
                    AGE to age_TextEdit.text.toString(),
                    HOBBIES to hobbies_TextEdit.text.toString()
               )
           )
            startActivity(intent)
        }
    }
}